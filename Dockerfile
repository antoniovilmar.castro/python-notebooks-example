FROM python:slim
RUN pip install jupyter
ADD notebooks notebooks
ENTRYPOINT ["jupyter","notebook", "--port=8888", "--notebook-dir=notebooks/", "--no-browser","--ip=0.0.0.0", "--allow-root"]
